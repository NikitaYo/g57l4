//
//  ViewController.swift
//  G57L4
//
//  Created by Nikita Kostash on 05.10.17.
//  Copyright © 2017 Nikita Kostash. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let number = Double(inputTextField.text!)!
        print(number)
        let intNum = Int(number)
       
        Tasks.helloWorld()
        Tasks.helloWorldCounts(count: 5)
        Tasks.daysOfWeeks(dayCount: 6)
        Tasks.independentDaysOfWeeks(inDayCount: 18)
        Tasks.inches(inch: 15.67)
        Tasks.lastTask(number: 33)
    }


}

