//
//  Tasks.swift
//  G57L4
//
//  Created by Nikita Kostash on 05.10.17.
//  Copyright © 2017 Nikita Kostash. All rights reserved.
//

import UIKit

class Tasks: NSObject {
    static func helloWorld() {
    print("Hello World!")
    }
    static func helloWorldCounts(count: Int) {
        for _ in 0..<count {
           print("Hello World Yo!")
        }
        
    }
    static func daysOfWeeks (dayCount: Int) {
        if dayCount == 1 {
            print("Понедельник")
        }
        else if dayCount == 2 {
            print("Вторник")
        }
        else if dayCount == 3 {
            print("Среда")
        }
        else if dayCount == 4 {
            print("Четверг")
        }
        else if dayCount == 5 {
            print("Пятница")
        }
        else if dayCount == 6 {
            print("Суббота")
        }
        else if dayCount == 7 {
            print("Воскресенье")
        }
        
        
    }
    
    static func independentDaysOfWeeks(inDayCount: Int) {
        
        if inDayCount == 1 {
            print("Понедельник")
        }
        else if inDayCount == 2 {
            print("Вторник")
        }
        else if inDayCount == 3 {
            print("Среда")
        }
        else if inDayCount == 4 {
            print("Четверг")
        }
        else if inDayCount == 5 {
            print("Пятница")
        }
        else if inDayCount == 6 {
            print("Суббота")
        }
        else if inDayCount == 7 {
            print("Воскресенье")
        }
        else if inDayCount%7 == 1 {
            print("Понедельник")
        }
        else if inDayCount%7 == 2 {
            print("Вторник")
        }
        else if inDayCount%7 == 3 {
            print("Среда")
        }
        else if inDayCount%7 == 4 {
            print("Четверг")
        }
        else if inDayCount%7 == 5 {
            print("Пятница")
        }
        else if inDayCount%7 == 6 {
            print("Суббота")
        }
        else if inDayCount%7 == 0 {
            print("Воскресенье")
        }
        
}
    static func inches (inch: Double) {
        let santimeters = inch * 2.54
        print("Сантиметры", santimeters)
    }
    static func lastTask(number: Int) {
        for i in 1..<101{
            if i % number == 0 {
                 print(i)
            }
           
        }
    }
    
    
    

}
